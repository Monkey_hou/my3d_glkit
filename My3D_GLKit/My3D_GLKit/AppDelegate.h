//
//  AppDelegate.h
//  My3D_GLKit
//
//  Created by 葡萄科技 on 2018/11/2.
//  Copyright © 2018年 Monkey_Hou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

